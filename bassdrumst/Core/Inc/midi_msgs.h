/*
 * midi_msgs.h
 *
 *  Created on: Nov 28, 2021
 *      Author: maor1
 */

#ifndef INC_MIDI_MSGS_H_
#define INC_MIDI_MSGS_H_

#include <stdint.h>



typedef enum{
	KickDrum = 36,
	NoteLast
} midiNotetypes;

typedef enum{
	VelLow = 20,
	VelMid = 60,
	VelHigh= 100,
	VelMax = 127,
	VelLast
} midiVeltypes;



typedef enum{
	NoteOn = 9,
	NoteOff= 8,
	MsgTypeLast
} midiMsgTypes;




typedef struct{
	union{
		uint8_t usbByte0;
		struct __attribute__ ((packed)){
			unsigned codeIndexNum:4;
			unsigned cableNum : 4;
		};

	};
	union{
		uint8_t midiByte0;
		struct __attribute__ ((packed)){
			unsigned channelNum : 4;
			unsigned msgType:4;
		};

	};

} midiusbHeaderType;



typedef struct{
	midiusbHeaderType header;
	uint8_t key;
	uint8_t vel;
}midiNoteMsgType;




void midiCreateNoteOnMessage(midiNotetypes eNote,midiVeltypes eVel);
void midiCreateNoteOffMessage(midiNotetypes eNote,midiVeltypes eVel);




#endif /* INC_MIDI_MSGS_H_ */
