/*
 * midi_msgs.c
 *
 *  Created on: 30 Nov 2021
 *      Author: maor1
 */


#include "midi_msgs.h"
#include "usbd_midi_if.h"






void midiCreateNoteOnMessage(midiNotetypes eNote,midiVeltypes eVel)
{
	midiNoteMsgType msg;


	msg.header.cableNum = 0;
	msg.header.codeIndexNum = NoteOn;
	msg.header.msgType = NoteOn;
	msg.header.channelNum = 0;

	msg.key = eNote;
	msg.vel = eVel;


	MIDI_DataTx((uint8_t*)&msg,sizeof(midiNoteMsgType));

}
void midiCreateNoteOffMessage(midiNotetypes eNote,midiVeltypes eVel)
{
	midiNoteMsgType msg;


	msg.header.cableNum = 0;
	msg.header.codeIndexNum = NoteOff;
	msg.header.msgType = NoteOff;
	msg.header.channelNum = 0;

	msg.key = eNote;
	msg.vel = eVel;


	MIDI_DataTx((uint8_t*)&msg,sizeof(midiNoteMsgType));

}
