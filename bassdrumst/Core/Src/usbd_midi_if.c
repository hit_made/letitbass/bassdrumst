/*
 *	11/26/17 by C.P.: Version 0.8.0 - Universal Version
 */

#include "usbd_midi_if.h"
#include "stm32l4xx_hal.h"
 

USBD_MIDI_ItfTypeDef USBD_Interface_fops_FS =
{
  MIDI_DataRx,
  MIDI_DataTx
};
 
uint16_t MIDI_DataRx(uint8_t *msg, uint16_t length)
{
  uint8_t chan = msg[0] & 0x0F;
  uint8_t msgtype = msg[0] & 0xF0;
  uint8_t b1 =  msg[2];
  uint8_t b2 =  msg[3];
  uint16_t b = ((b2 & 0x7F) << 7) | (b1 & 0x7F);
 

  return 0;
}

uint16_t MIDI_DataTx(uint8_t *msg, uint16_t length)
{
  uint32_t i = 0;
  //fixme: might cause buffer overflow
  while (i < length) {
    APP_Rx_Buffer[APP_Rx_ptr_in] = *(msg + i);
    APP_Rx_ptr_in++;
    i++;
    if (APP_Rx_ptr_in == APP_RX_DATA_SIZE) {
      APP_Rx_ptr_in = 0;
    }
  }
  USBD_MIDI_SendPacket();
  return USBD_OK;
}
